<?php
$q = $_GET['q'];

$con = mysqli_connect('localhost','root','','agenda');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

$sql="SELECT usuario.codigo, usuario.nombre, usuario.apellidos, pendiente.titulo
from usuario 
inner join pendiente ON  usuario.id = pendiente.idUsuario
Where nombre LIKE '%$q%'";
$result = mysqli_query($con,$sql);

echo "<div class='table-responsive-sm'>
<table class='table table-bordered table-hover '>
<thead class='thead-dark'>
    <tr class='bg-primary'>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Titulo</th>
    </tr>
</thead>";
while($row = mysqli_fetch_array($result)) {
    
    echo "<tr>";
    echo "<td>" . $row['codigo'] . "</td>";
    echo "<td>" . $row['nombre'] . "</td>";
    echo "<td>" . $row['apellidos'] . "</td>";
    echo "<td>" . $row['titulo'] . "</td>";
    echo "</tr>";
}
echo "</table></div>";
mysqli_close($con);
?>
